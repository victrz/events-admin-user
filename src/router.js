import Vue from 'vue'
import Router from 'vue-router'
import HomeWrap from '@/components/HomeWrap'
import LoginMain from '@/components/login/LoginMain'
import LocationsHome from '@/components/LocationsHome'
import EventsCreate from '@/components/EventsCreate'
import EventsViewAllLocation from '@/components/EventsViewAllLocation'
import Categories from '@/components/Categories'
import Tags from '@/components/Tags'

Vue.use(Router)
Vue.component('HomeWrap', HomeWrap)
Vue.component('LoginMain', LoginMain)
Vue.component('LocationsHome', LocationsHome)
Vue.component('EventsCreate', EventsCreate)
Vue.component('EventsViewAllLocation', EventsViewAllLocation)
Vue.component('Categories', Categories)
Vue.component('Tags', Tags)

export default new Router({
  routes: [
    {
      path: '/',
      // redirect: '/login'
      component: HomeWrap,
      name: 'HomeWrap',
      // name: 'Home'
      children: [
        {
          path: 'locations',
          component: LocationsHome
        },
        {
          path: 'create',
          component: EventsCreate
        },
        {
          path: 'view-events',
          component: EventsViewAllLocation
        },
        {
          path: '/categories',
          component: Categories
        },
        {
          path: '/tags',
          component: Tags
        },
      ]
    },
    {
      path: '/login',
      name: 'LoginMain',
      component: LoginMain,
      meta: {
        permissions: []
      }
    },
    {
      path: '/login/:expired',
      component: LoginMain,
      name: 'LoginExpired',
      meta: {
        permissions: []
      }
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: LoginMain,
      meta: {
        permissions: []
      }
    }
    // {
    //   path: '/locations',
    //   name: 'LocationsHome',
    //   component: LocationsHome,
    //   meta: {
    //     permissions: []
    //   }
    // }
  ]
})
