import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import Env from '../environment.js'
import axios from 'axios'
Vue.use(Vuex)

const store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    login: {
      token: '',
      user_id: null,
      app_id: '',
      app_secret: ''
    },
    activeLocation: {},
    activeEventId: null,
    allCategories: [],
    allTags: [],
    ifIsEdit: false
  },
  mutations: {
    SET_VUEX_STORAGE (state, data) {
      state.login.token = data.token
      state.login.user_id = data.user_id
      state.login.app_secret = data.app_secret
      state.login.app_id = data.app_id
    },
    ACTIVE_LOCATION (state, data) {
      state.activeLocation = data.location
    },
    CATEGORIES (state, data) {
      state.allCategories = data.categories
    },
    TAGS (state, data) {
      state.allTags = data.tags
    },
    EDIT (state, data) {
      console.log(data.edit)
      state.ifIsEdit = data.edit
    },
    ACTIVE_EVENT_ID (state, data) {
      console.log(data.id)
      state.activeEventId = data.id
    }
  },
  actions: {
    SET_STORAGE ({commit}, data) {
      commit('SET_VUEX_STORAGE', data)
    },
    SET_ACTIVE_LOCATION ({commit}, data) {
      console.log(data)
      commit('ACTIVE_LOCATION', data)
    },
    SET_CATEGORIES ({commit}, data) {
      commit('CATEGORIES', data)
    },
    SET_TAGS ({commit}, data) {
      commit('TAGS', data)
    },
    SET_EDIT ({commit}, data) {
      console.log('are we here?')
      console.log(data)
      commit('EDIT', data)
    },
    SET_ACTIVE_EVENT_ID ({commit}, data) {
      console.log(data)
      commit('ACTIVE_EVENT_ID', data)
    },
    RETURN_ALL_TAGS ({state}) {
      return axios({
        method: 'POST',
        url: `${Env.url}/tag/get_all_tags`,
        headers: {
          // 'Content-Type': 'application/json',
          'app-id': `${Env.app_id}`,
          'app-secret': `${Env.app_secret}`,
          'token': state.login.token
        }
      })
    },
    RETURN_ALL_CATEGORIES ({state}) {
      return axios({
        method: 'POST',
        url: `${Env.url}/category/get_all_categories`,
        headers: {
          // 'Content-Type': 'application/json',
          'app-id': `${Env.app_id}`,
          'app-secret': `${Env.app_secret}`,
          'token': state.login.token
        }
      })
    }
  },
  getters: {
    getToken (state) {
      return state.login.token
    },
    getCategories (state) {
      return state.allCategories
    },
    getTags (state) {
      return state.allTags
    },
    getActiveLocation (state) {
      return state.activeLocation
    },
    getIfEditEvent (state) {
      return state.ifIsEdit
    },
    getActiveEventId (state) {
      return state.activeEventId
    },
    // getTimerIds (state) {
    //   return state.timerIds
    // }
  }
})

export default store
