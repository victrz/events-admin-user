import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router.js'
import BulmaExtensions from 'bulma-extensions'
import 'bulma-extensions/dist/css/bulma-extensions.min.css'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale'
import lang from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'

//Font awesome
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import Tag from 'v-tag-suggestion'
Vue.component('icon', Icon)
Vue.component('tag', Tag)

require('./assets/css/main.scss');
Vue.use(BulmaExtensions)
Vue.use(ElementUI)
locale.use(lang)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
